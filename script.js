let initTrainer = 
{

	Name: "Ash Kechum",
	Age:10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	Friends: 
		{

			hoen: ["May", "Max"],
			kanto: ["Brock", "Cristy"]

		}
}

function Trainer (personName, personAge, preferredPokemon, bff) 
{
	
	this.Name = personName;
	this.Age = personAge;
	this.Pokemon = preferredPokemon;
	this.Friends = bff

}

let talk = new Trainer (
						"Ash Kechum",
						10,
						["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
						{

							hoen: ["May", "Max"],
							kanto: ["Brock", "Cristy"]

						}
					   )

console.log (talk);

console.log (`Result of the talk method:`)

console.log (initTrainer.Pokemon[0], `I choose you!`)

console.log (`Result of dot notation:`);

console.log (initTrainer.Name);

console.log (`Result of square bracket notation:`);

console.log (initTrainer.Pokemon);

function Pokemon (pName,pLvl, hp) 
{

	this.pokemonName = pName;
	this.pokemonLevel = pLvl;
	this.health = hp * 5;
	this.attack = hp * 10;
	this.tackle = function (target) 
	{
		let damage = Math.floor(Math.random() * (target.health - 5 + 1)) + 5;
		

		if (target.health == 0) {
			console.log (`Pokemon fainted`);
		} else {
			target.health -= damage;
		}
	}
	this.faint = function (target)
	{
		console.log (`Pokemon fainted`);
	}

}

let Celebi = new Pokemon ("Celebi", 12, 5500); //grass type pokemon
let Torchic = new Pokemon ("Torchic", 12, 5500); //fire type pokemon

Celebi.tackle (Torchic);
		   